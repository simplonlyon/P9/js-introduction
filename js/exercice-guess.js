





const numberToGuess = 10;
let answer = parseInt(prompt("Essayes une nombre"));
let tries = 1;
const maxTries = 5;

while (numberToGuess !== answer && tries <= maxTries) {
    // if (tries === maxTries) {
    //     alert('game over');
    //     break;
    // }
    if (answer < numberToGuess) {
        answer = parseInt(prompt("C'est plus grand"));
    }
    else if (answer > numberToGuess) {
        answer = parseInt(prompt("C'est plus petit"));
    }
    else {
        answer = parseInt(prompt("Oupsi, tapes un chiffre stp"));
    }
    tries++;
}
if (tries <= maxTries) {
    alert("Bravo, tu as trouvé en " + tries + " essais");
}else {
    alert('Game over');
}