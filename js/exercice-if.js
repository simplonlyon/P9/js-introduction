
/* 1. Créer une variable avec du texte dedans puis faire une condition
    pour afficher la valeur de la variable en console dans le cas où
    cette variable contient "bloup"
    2. La même chose avec une variable numérique, et cette fois on
    affichera sa valeur seulement si la valeur est supérieure à 10
    3. Faire un autre if qui affichera un truc si la variable numérique
    a une valeur entre 5 et 10 non compris
    4. Faire encore un autre if qui affichera un truc si la variable
    numérique a une valeur entre 20 et 100 compris ou vaut 15

*/

let exo1 = 'osef';

if(exo1 === 'bloup') {
    console.log(exo1);
} else {
   console.log("exo1 not equal bloup");
    
}

let exoNum = 15;

if(exoNum > 10) {
    console.log("greater than 10");
}

if(exoNum > 5 && exoNum < 10) {
    console.log("between 5 and 10");
}

if( (exoNum >= 20 && exoNum <= 100) || exoNum === 15) {
    console.log("un truc");
}