/*
Utilisez des boucles pour faire une pyramide ressemblant
peu ou prou à ça en console.log :
    *
   ***
  *****
 *******
*********

Good Luck
*/


let countLevel = 5; // 4   // 3     // 2
let starSave = `*`; // *** // ***** // ********


while (0 < countLevel) {
   let line = ``;
   let countSpace = countLevel;
   while (0 < countSpace) {
      line += ` `;
      countSpace--;
   }
   line += starSave;
   console.log(line);

   starSave += `**`;
   countLevel--;
}

let levelNumber = 5;
let levelSize = levelNumber * 2 - 1;

for(let x = 0; x < levelNumber; x++) {
    let line = '';

    let starNumber = x * 2 + 1;
    let spaceNumber = (levelSize - starNumber) / 2;

    for(let y = 0; y < spaceNumber; y++) {
        line+= ' ';
    }

    for(let y = 0; y < starNumber; y++) {
        line+= '*';
    }


    console.log(line);
}

/*
let countLevel = 5; // 4   // 3     // 2
let starSave = 1; // *** // ***** // ********


while (0 < countLevel) {
   let line = ` `.repeat(countLevel);
   line += `*`.repeat(starSave);
   console.log(line);

   starSave += 2;
   countLevel--;
}

*/