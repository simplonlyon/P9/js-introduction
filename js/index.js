/**
 * Une variable en gros c'est une boîte nommée dans laquelle on stock des valeurs
 * Une valeur peut être de différent types :
 * Primitifs 
 * - number : une valeur numérique, à virgule ou non
 * - string : une chaîne de caractère, entre '' ou "" ou ``
 * - boolean : true ou false
 * - null : une valeur nulle
 * - undefined : la valeur par défaut de tout ce qui n'existe pas, "non définie"
 * Complexe
 * - array : un ensemble de valeur, genre [1,2,3,4]
 * - function : une fonction
 * - object : un objet complexe (avec des propriétés et des méthodes)
 * 
 * Pour ne pas se perdre, on fera en sorte que lorsqu'on met un type
 * dans une variable, on ne vienne pas changer son type plus loin, par exemple
 * si on fait une variable avec 1 dedans, on évitera de mettre "coucou" dedans plus tard
 * 
 * Pour le nommage de la variable, on utilisera le camelCase, en faisant
 * en sorte de donnée un nom parlant (mais pas trop spécifique) à la variable
 * (se demander "est-ce qu'une personne qui ne me connait pas pourrait savoir le contenu de cette variable juste avec son nom ?")
 */

//en js, on utilise le let pour déclarer une variable
let first = 1;
//Une const est une constante, sa valeur ne peut pas être modifiée
const second = 1;

first = 2; //ok
//second = 2; //erreur

//On peut faire des calculs mathématiques
let result = 1 + 2 * 3 / 2 - 2;
//Afficher des informations dans la console du navigateur avec console.log
// console.log(result);
console.log(1 !== 2);

/**
 * Les opérateurs logiques sont en gros des "comparateurs" de valeur
 * qui renveront true ou false selon les valeurs
 */
//true si A supérieur à B
let logicGreaterThan = 1 > 2; //false
//true si A inférieur à B
let logicLesserThan = 1 < 2; //true
//true si A supérieur ou égal à B
let logicGreaterThanOrEqual = 1 >= 2; //false
//true si A strictement égal à B (en type et en valeur)
let logicEqual = 1 === 1; //true
//true si A n'est pas égal à B
let logicNotEqual = 1 !== 1; //false
//true si A est true et que B est true également
let logicAnd = 1 === 1 && 2 === 1; //false
//true si A est true et/ou que B est true
let logicOr = 1 === 1 || 2 === 1; //true
// ! permet de dire "l'inverse de..."
let logicNot = !true; //false


if(true) {
    console.log('coucou');
}