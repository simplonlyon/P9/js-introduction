/**
 * Les boucles (loops) vont permettre de répéter une instruction un
 * certain nombre de fois. A chaque tour, une variable témoin changera
 * de valeur et modifiera donc l'instruction exécutée
 * 
 * side note incrémentation : Les lignes suivantes font strictement la même chose
 * variable++;
 * variable += 1;
 * variable = variable + 1;
 */

//Le for déclare une variable, puis la condition d'arrêt, puis l'action à effectuer à chaque fin de tour
for (let x = 0; x < 10; x++) {

    //A chaque tour de boucle, on affichera la valeur actuelle de x

    //On fait ici ce qu'on appelle une concaténation, cela consiste à
    //ajouter la valeur d'une variable dans une chaîne de caractères.
    //Lorsqu'on utilise les "" ou '', il faut concaténer avec +
    console.log('x = ' + x + ' !');
    //Lorsqu'on utilise les backtick, on peut mettre directement la 
    //variable entre ${}
    console.log(`x = ${x} !`);

}
//On déclare une variable compteur
let counter = 0;
//On fait un while en lui disant sa condition d'arrêt (booléenne)
while (counter < 10) {
    //On affiche le compteur en console
    console.log(counter);
    //On incrémente le compteur de 1 à la fin de chaque tour
    counter++;
}

//Même boucle, mais en affichant 0 1 2 3 4 5 6 7 8 9 en un seul console.log
let counter2 = 0;
let count = "";
while (counter2 < 10) {
    count += counter2 + " ";

    counter2++;
}
console.log(count);

let phrase = "coucou";
//On peut ajouter du texte au bout d'une chaîne de caractère avec +=
phrase += " tout le monde";
phrase = phrase + " tout le monde";
console.log(phrase);


//On lance une première boucle de 10 tours
for (let x = 0; x < 10; x++) {
    //à chaque tour, elle lance elle même une boucle de 10
    for(let y = 0; y < 10; y++) {
        //ici, coucou sera donc affiché 100 fois pasque 10 x 10        
        console.log("coucou");
    }
    
}